<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://folsomcreative.com
 * @since             1.0.0
 * @package           Folsom_Widgets_Bundle
 *
 * @wordpress-plugin
 * Plugin Name:       Folsom Widgets Bundle
 * Plugin URI:        https://folsomcreative.com
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.4.0
 * Author:            Folsom Creative, Inc.
 * Author URI:        https://folsomcreative.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       folsom-widgets-bundle
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'FWB_VERSION', '1.4.0' );

require_once plugin_dir_path( __FILE__ ) . 'lib/wp-package-updater/class-wp-package-updater.php';

$folsom_updater = new WP_Package_Updater(
  'https://plugins.folsomcreative.com',
  wp_normalize_path( __FILE__ ),
  wp_normalize_path( plugin_dir_path( __FILE__ ) )
);

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-folsom-widgets-bundle-activator.php
 */
function activate_folsom_widgets_bundle() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-folsom-widgets-bundle-activator.php';
	Folsom_Widgets_Bundle_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-folsom-widgets-bundle-deactivator.php
 */
function deactivate_folsom_widgets_bundle() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-folsom-widgets-bundle-deactivator.php';
	Folsom_Widgets_Bundle_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_folsom_widgets_bundle' );
register_deactivation_hook( __FILE__, 'deactivate_folsom_widgets_bundle' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-folsom-widgets-bundle.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_folsom_widgets_bundle() {

	require_once(__DIR__ . '/vendor/autoload.php');
	$timber = new \Timber\Timber();

	$plugin = new Folsom_Widgets_Bundle();
	$plugin->run();

}
run_folsom_widgets_bundle();
