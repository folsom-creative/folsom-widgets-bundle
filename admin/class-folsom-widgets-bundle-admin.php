<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://folsomcreative.com
 * @since      1.0.0
 *
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/admin
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Folsom_Widgets_Bundle_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		add_filter( 'siteorigin_widgets_widget_folders', [ &$this, 'add_widget_folder' ] );
		add_filter( 'siteorigin_panels_widget_dialog_tabs', [ &$this, 'add_widget_tab' ], 20 );
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Folsom_Widgets_Bundle_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Folsom_Widgets_Bundle_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/folsom-widgets-bundle-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Folsom_Widgets_Bundle_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Folsom_Widgets_Bundle_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/folsom-widgets-bundle-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Makes the plugin's widgets available to Page Builder.
	 * 
	 * @since    	1.0.0
	 * @param		array		$folders	The folders array from Page Builder.
	 */
	function add_widget_folder( $folders ) {
		$folders[] = plugin_dir_path(__FILE__) . '../widgets/';
		return $folders;
	}

	/**
	 * Adds a Folsom Widgets tab to Page Builder.
	 * 
	 * @since		1.0.0
	 * @param		array		$tabs		The tabs array from Page Builder.
	 */	  
	function add_widget_tab( $tabs ) {
		$tabs[] = [
			'title' => __( 'Folsom Widgets', 'folsom' ),
			'filter' => [
				'groups' => [
					'folsom'
				]
			]
		];

		return $tabs;
	}		  

}