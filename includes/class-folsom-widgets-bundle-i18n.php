<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://folsomcreative.com
 * @since      1.0.0
 *
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/includes
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Folsom_Widgets_Bundle_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'folsom-widgets-bundle',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
