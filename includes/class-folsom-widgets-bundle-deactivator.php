<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://folsomcreative.com
 * @since      1.0.0
 *
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/includes
 * @author     Folsom Creative, Inc. <hello@folsomcreative.com>
 */
class Folsom_Widgets_Bundle_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
