<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://folsomcreative.com
 * @since      1.0.0
 *
 * @package    Folsom_Widgets_Bundle
 * @subpackage Folsom_Widgets_Bundle/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
