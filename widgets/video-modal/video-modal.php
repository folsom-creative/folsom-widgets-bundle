<?php
/*
Widget Name: Video Modal
Description: Display a video in a modal with a custom thumbnail.
Author: Folsom Creative, Inc.
Author URI: https://www.folsomcreative.com/
*/

class Folsom_Widgets_Video_Modal_Widget extends SiteOrigin_Widget {

	function __construct() {
		parent::__construct(
			'folsom-video-modal',
			__( 'Video Modal', 'folsom-widgets-bundle' ),
			[
				'description' => __( 'Embed a Youtube or Vimeo video in a modal with a custom thumbnail.', 'folsom-widgets-bundle' ),
				'panels_groups' => [ 'folsom' ],
			],
			[

			],
			false,
			plugin_dir_path( __FILE__ )
		);
	}

	function initialize() {
		$this->register_frontend_styles( [
			[
				'folsom-video-modal-css',
				plugin_dir_url(__FILE__) . 'css/style.css'
			],
			[
				'fancybox-css',
				plugin_dir_url(__FILE__) . 'css/jquery.fancybox.min.css'
			]
		 ] );
		$this->register_frontend_scripts(
			[
				[
					'fancybox-js',
					plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.min.js',
					[ 'jquery' ]
				]
			]
		);
	}

	function get_widget_form() {
		return [
			'title' => [
				'type' => 'text',
				'label' => __('Title', 'folsom-widgets-bundle'),
			],
			'image' => [
				'type' => 'media',
				'label' => __('Image', 'folsom-widgets-bundle'),
				'description' => __('The best image has a 16 by 9 aspect ratio. The best image is 800px wide by 450px tall. Uncropped images may slow down page speed.', 'folsom-widgets-bundle' ),
			],
			'url' => [
				'type' => 'text',
				'label' => __('Video URL', 'folsom-widgets-bundle'),
				'description' => __('Enter a Youtube or Vimeo URL', 'folsom-widgets-bundle'),
			],
			'button_theme' => [
				'type' => 'radio',
				'label' => __('Play Button Theme', 'folsom-widgets-bundle'),
				'description' => __('Choose a light theme for a white button and dark theme for a black button.', 'folsom-widgets-bundle'),
				'default' => 'light',
				'options' => [
					'light' => __( 'Light Theme', 'folsom-widgets-bundle' ),
					'dark' => __( 'Dark Theme', 'folsom-widgets-bundle' ),
				],
			]
		];
	}

	function get_template_variables( $instance, $args ) {
		return [
			'video' => ! empty( $instance['video'] ) ? $instance['video'] : [],
		];
	}

	function video_thumbnail( $image_id ) {
		if ( ! empty( $image_id ) ) {
			$src = wp_get_attachment_image_src( $image_id, 'full' );
			return esc_url( $src[0] );
		}
	}

}

siteorigin_widget_register( 'folsom-video-modal', __FILE__, 'Folsom_Widgets_Video_Modal_Widget' );
