<?php
/*
Widget Name: Restaurant Menu
Description: A list of menu items.
Author: Chris Lanphear, Folsom Creative, Inc.
Author URI: https://folsomcreative.com
*/
class Restaurant_Menu extends SiteOrigin_Widget {
  protected $widget_id = 'restaurant-menu';

  function __construct() {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.

    // Set our icon picker to only use FontAwesome for this widget
    // add_filter( 'siteorigin_widgets_icon_families', [ &$this, 'modify_siteorigin_use_fa_only' ] );

    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        $this->widget_id,

        // The name of the widget for display purposes.
        __('Restaurant Menu', 'folsom-widgets-bundle'),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
            'description' => __('A list of menu items.', 'folsom-widgets-bundle'),
            // 'help'        => 'http://example.com/hello-world-widget-docs',
            'panels_groups' => [ 'folsom' ],
            'panels_icon' => 'dashicons dashicons-carrot',
        ),

        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        [
          'sections' => [
            'type' => 'repeater',
            'label' => __( 'Sections', 'folsom-widgets-bundle' ),
            'item_name' => __( 'Section', 'folsom-widgets-bundle' ),
            'item_label' => [
              'selector' => "[id*='title']",
              'update_event' => 'change',
              'value_method' => 'val',
            ],
            'fields' => [
              'title' => [
                'type' => 'text',
                'label' => __( 'Section Title', 'folsom-widgets-bundle' ),
              ],
              'description' => [
                'type' => 'textarea',
                'label' => __( 'Description', 'folsom-widgets-bundle' ),
              ],
              'image' => [
                'type' => 'media',
                'label' => __( 'Image', 'folsom-widgets-bundle' ),
              ],
              'avail_start' => [
                'type' => 'text',
                'label' => __( 'Start Time', 'folsom-widgets-bundle' ),
                'description' => __( 'Enter HH:MM in 24-hour format, ie. 15:00', 'folsom-widgets-bundle' ),
              ],
              'avail_end' => [
                'type' => 'text',
                'label' => __( 'End Time', 'folsom-widgets-bundle' ),
                'description' => __( 'Enter HH:MM in 24-hour format, ie. 15:00', 'folsom-widgets-bundle' ),
              ],
              'dishes' => [
                'type' => 'repeater',
                'label' => __( 'Dishes', 'folsom-widgets-bundle' ),
                'item_name' => __( 'Dish', 'folsom-widgets-bundle' ),
                'item_label' => [
                  'selector' => "[id*='name']",
                  'update_event' => 'change',
                  'value_method' => 'val',
                ],
                'fields' => [
                  'name' => [
                    'type' => 'text',
                    'label' => __( 'Name', 'folsom-widgets-bundle' ),
                  ],
                  'description' => [
                    'type' => 'textarea',
                    'label' => __( 'Description', 'folsom-widgets-bundle' ),
                  ],
                  'price' => [
                    'type' => 'number',
                    'label' => __( 'Price', 'folsom-widgets-bundle' ),
                  ],
                  'nutrition' => [
                    'type' => 'section',
                    'label' => __( 'Nutrition', 'folsom-widgets-bundle' ),
                    'optional' => true,
                    'hide' => true,
                    'fields' => [
                      'calories' => [
                        'type' => 'number',
                        'label' => __( 'Calories', 'folsom-widgets-bundle' ),
                      ],
                      'fat_content' => [
                        'type' => 'number',
                        'label' => __( 'Fat Content', 'folsom-widgets-bundle' ),
                      ],
                      'fiber_content' => [
                        'type' => 'number',
                        'label' => __( 'Fiber Content', 'folsom-widgets-bundle' ),
                      ],
                      'protein_content' => [
                        'type' => 'number',
                        'label' => __( 'Protein Content', 'folsom-widgets-bundle' ),
                      ],
                    ]
                  ]
                ]
              ],              
            ],
          ],
          'display' => [
            'type' => 'section',
            'label' => __( 'Display', 'folsom-widgets-bundle' ),
            'fields' => [
              'accent' => [
                'type' => 'radio',
                'label' => __( 'Accent', 'folsom-widgets-bundle' ),
                'options' => [
                  'dots' => 'Dots',
                  'none' => 'None',
                ]
              ],
              'dish_background_color' => [
                'type' => 'color',
                'label' => __( 'Dish Background Color', 'folsom-widgets-bundle' ),
                'default' => '#ffffff',
              ]
            ]
          ]
        ],

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }

  function get_less_variables( $instance ) {
    return [
      'dish_background_color' => $instance['display']['dish_background_color'],
    ];
  }

  function get_template_name( $instance ) {
    return 'template-' . $this->widget_id;
  }

  function get_style_name( $instance ) {
    return $this->widget_id;
  }
}

siteorigin_widget_register( 'restaurant-menu', __FILE__, 'Restaurant_Menu' );
