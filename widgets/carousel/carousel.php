<?php
/*
Widget Name: Carousel
Description: A rotating selection of items.
Author: Chris Lanphear, Folsom Creative, Inc.
Author URI: https://folsomcreative.com
*/
class Carousel extends SiteOrigin_Widget {
  protected $widget_id = 'carousel';

  function __construct() {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.

    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        $this->widget_id,

        // The name of the widget for display purposes.
        __('Carousel', 'folsom-widgets-bundle'),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
            'description' => __('A rotating selection of items.', 'folsom-widgets-bundle'),
            // 'help'        => 'http://example.com/hello-world-widget-docs',
            'panels_groups' => [ 'folsom' ],
            'panels_icon' => 'dashicons dashicons-images-alt',
        ),

        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        [
          'items' => [
            'type' => 'repeater',
            'label' => __( 'Items', 'folsom-widgets-bundle' ),
            'item_name' => __( 'Item', 'folsom-widgets-bundle' ),
            'item_label' => [
              'selector' => "[id*='title']",
              'update_event' => 'change',
              'value_method' => 'val',
            ],
            'fields' => [
              'title' => [
                'type' => 'text',
                'label' => __( 'Title', 'folsom-widgets-bundle' ),
              ],
              'link' => [
                'type' => 'text',
                'label' => __( 'Link to URL', 'folsom-widgets-bundle' ),
                'optional' => true,
                'sanitize' => 'url',
              ],
              'image' => [
                'type' => 'media',
                'label' => __( 'Image', 'folsom-widgets-bundle' ),
              ],
            ],
          ],
          'display' => [
            'type' => 'section',
            'label' => __( 'Display Options', 'folsom-widgets-bundle' ),
            'fields' => [
              'items' => [
                'type' => 'number',
                'label' => __( 'Items on screen (desktop)', 'folsom-widgets-bundle' ),
                'default' => 3,
              ],
              'items_mobile' => [
                'type' => 'number',
                'label' => __( 'Items on screen (mobile)', 'folsom-widgets-bundle' ),
                'default' => 3,
              ],
              'items_tablet' => [
                'type' => 'number',
                'label' => __( 'Items on screen (tablet)', 'folsom-widgets-bundle' ),
                'default' => 3,
              ],
              'margin' => [
                'type' => 'number',
                'label' => __( 'Margin', 'folsom-widgets-bundle' ),
              ],
              'loop' => [
                'type' => 'checkbox',
                'label' => __( 'Loop', 'folsom-widgets-bundle' ),
              ],
              'center' => [
                'type' => 'checkbox',
                'label' => __( 'Center item(s)', 'folsom-widgets-bundle' ),
              ],
              'nav' => [
                'type' => 'checkbox',
                'label' => __( 'Show navigation buttons', 'folsom-widgets-bundle' ),
              ],
              'dots' => [
                'type' => 'checkbox',
                'label' => __( 'Show dots navigation', 'folsom-widgets-bundle' ),
              ],
              'autoplay' => [
                'type' => 'checkbox',
                'label' => __( 'Autoplay', 'folsom-widgets-bundle' ),
              ],
              'autoplay_timeout' => [
                'type' => 'number',
                'label' => __( 'Autoplay Speed', 'folsom-widgets-bundle' ),
                'description' => 'In seconds.',
              ],
              'show_titles' => [
                'type' => 'checkbox',
                'label' => __( 'Show item titles', 'folsom-widgets-bundle' ),
              ],
            ]
          ]
        ],

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }

  function initialize() {
		$this->register_frontend_styles([
			[
				'owl-carousel-css',
				'//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css'
      ],
      [
				'owl-carousel-theme-css',
				'//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css'
			]
     ]);
     
		$this->register_frontend_scripts([
				[
					'owl-carousel',
					'//cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js',
					[ 'jquery' ]
				]
			]);
	}

  function get_less_variables( $instance ) {
    return [
      'dish_background_color' => $instance['display']['dish_background_color'],
    ];
  }

  function get_template_name( $instance ) {
    return 'template-' . $this->widget_id;
  }

  function get_style_name( $instance ) {
    return $this->widget_id;
  }
}

siteorigin_widget_register( 'carousel', __FILE__, 'Carousel' );
