<?php
$context = Timber::get_context();
$context['instance'] = $instance;
$context['instance']['settings'] = get_option( 'siteorigin_panels_settings' );
Timber::render( 'Carousel.twig', $context );
